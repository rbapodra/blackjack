#pragma once

#ifndef CARDSTACK_H
#define CARDSTACK_H


#include <vector>


#include "Card.h"
//#include "Player.h"
//#include "Dealer.h"

using namespace std;


class Player;
class Dealer;
class Card;

class CardStack
{
private:

    unsigned int m_cardtexture[53];
	vector<Card> m_CardDeck;
	int pCardDeckIndices[52];
	

	int top;
	int facedownindex;
public:
	
	CardStack(void);
	~CardStack(void);
	void Initialize();
	void Shuffle();
	void dealCards(Player* pPlayer,Dealer* pDealer);
	void DrawCard(Player* pPlayer,Dealer* pDealer);
	void LoadCardTexture();
	void RenderCard(int index);
	bool CheckVisibility(int index,int& position);
	void SetCardVisibility(int index,bool visible);
	bool CheckFaceDown(int index);
	int GetFaceDownIndex() {return facedownindex; }
	void Reset();
};

#endif