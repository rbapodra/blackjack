/////Definition for the Player.h functions


#include "Player.h"

Player::Player()
{
	m_Hand.reserve(2);
	m_ChoicesLeft = 10;
	m_handValue = 0;
	m_playerAceCount = 0;
	m_AmountBet = 0;
	m_Balance = 2000;
}

Player::~Player()
{
	
}

bool Player::TestforBlackJack()
{
	if(m_handValue == 21 && (m_Hand.at(0).isAce() || m_Hand.at(1).isAce()) )	// keep a flag also because 21 has to occur in the 1st 2 cards only
	{	
			return true;
	}
	else return false;
}

void Player::OnHitPlayer(CardStack *pCardDeck)
{
	pCardDeck->DrawCard(this,NULL);
}

void Player::OnDoubleDownPlayer(CardStack *pCardDeck)
{
	OnHitPlayer(pCardDeck); // player can hit only once
	//OnStandPlayer 
	//transfer control to dealer
}

void Player::SetBetAmount(int amount)
{
	m_AmountBet += amount;
}

void Player::SetBalance(int index)
{
	if(index == 1)
		m_Balance -= m_AmountBet;

	else if(index == 2)
		m_Balance += m_AmountBet;
}

void Player::SetHandValue(int value)
{
	m_handValue = m_handValue + value;
}

bool Player::isBusted()
{
	if(m_handValue > 21)
		return true;
	else return false;
}

void Player::Reset()
{
	m_Hand.clear();
	m_handValue = 0;
	m_playerAceCount = 0;
	m_AmountBet = 0;

}