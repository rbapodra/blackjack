#pragma once

#include "BJButton.h"
#include "CardStack.h"

class BJButton;
class CardStack;


class CGfxOpenGL
{
public:
	void Render();
	void Prepare(float dt);

	void SetupProjection(int width, int height);

	bool ShutDown();
	bool Init();

	float GetAngle(){return m_angle;}
	int GetWindowHeight(){return m_windowHeight;}
	int GetWindowWidth(){return m_windowWidth;}
	int GetWindowBits(){return m_windowBits;}

	unsigned int CreateMyFont(char* fontName, int fontSize);
	void RenderFont(int xPos,int yPos, unsigned int base,char *str);
	void ReleaseFont(unsigned int base);
	void RenderHandValue();
	void SetPlayerHandValue(int value) { playerhandValue = value; }
	void SetDealerHandValue(int value) { dealerhandValue = value; }
	void SetPlayerBalanceValue(int value) {playerbalanceValue = value; }
	void SetPlayerBetValue(int value) {playerbetamt = value; }
	void SetPlayerChancesValue(int value) {playerchances = value; }
	void RenderBettingChips();
	void LoadBettingChips();
	void RenderTextMessage();
	void SetRenderTextIndex(int index) { textindex = index; }
	void SetRenderChipsIndex(bool render) { renderchips = render; }

	CGfxOpenGL(void);
	virtual ~CGfxOpenGL(void);
	
	BJButton dealbutton;
	BJButton quitbutton;
	BJButton hitbutton;
	BJButton standbutton;
	BJButton doubledownbutton;
	BJButton okbutton;
	CardStack m_Cards;
	
	
private:
	float m_angle;
	int m_windowHeight;
	int m_windowWidth;
	int m_windowBits;
	unsigned int m_fontListBase;
	unsigned int m_textListBase;
	unsigned int m_chiptexture[3];
	int playerhandValue;
	int dealerhandValue;
	int playerbalanceValue;
	int playerbetamt;
	int playerchances;
	int textindex;
	bool renderchips;
};

//GLvoid BuildFont(GLvoid);
//GLvoid glPrint(const char *text,...);