#pragma once

#include "CardStack.h"
#include "Card.h"
#include <vector>

using namespace std;

class CardStack;
class Card;

class Dealer
{
private:
	vector<Card> m_Hand;
	int m_handValue;
	int m_dealerAceCount;
public:
	Dealer(void);
	~Dealer(void);
	vector<Card> GetHand() { return m_Hand; }
	int GetHandValue() { return m_handValue; }
	void Hit(CardStack* pCardDeck);
	void SetHandValue(int value);
	bool isBusted();
	bool TestForBlackJack();
	void Reset();
	int GetAceCount() {return m_dealerAceCount; }
	void SetAceCount() {m_dealerAceCount++; }
};
