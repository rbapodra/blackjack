#pragma once

#include "CardStack.h"
#include "Player.h"
#include "Dealer.h"

class CardStack;
class Player;
class Dealer;

class BlackJack
{
private:
	
	bool m_GameStatus;
	int Minimum(int DealerHandValue,int PlayerHandValue);
public:
	BlackJack(void);
	~BlackJack(void);
	int GameWinCheck();
	bool GetGameStatus(){return m_GameStatus; }
	//Player GetPlayer() {return m_player; }
	//CardStack GetCards() {return m_Cards; }
	//Dealer GetDealer() {return m_dealer; }
	void SetGameStatus(bool status) { m_GameStatus = status; }
	
	//CardStack m_Cards;
	Player m_player;
	Dealer m_dealer;
};
