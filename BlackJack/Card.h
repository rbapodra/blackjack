/// declaration for Card object

#pragma once

#ifndef CARD_H
#define CARD_H

enum Suit {clubs = 1,spades,hearts,diamonds };

class Card
{
public:
	Card();
	~Card();
	bool isFaceDown();
	void FaceDown(bool val);
	Suit GetSuit() {return m_suit; }
	int GetValue() {return value; }
	void SetValue(int v) { value = v; }
	void SetSuit(Suit s) { m_suit = s; }
	bool isAce();
	bool isVisible();
	void SetVisibility(bool visible);
	void SetOffset(int o) { m_offset = o; }
	void SetOwner(int own) { m_owner = own; }
	int GetOwner() { return m_owner; }
	int GetOffset() { return m_offset; }
	
private:
	Suit m_suit;
	int value;
	bool m_visibility;
	int m_offset;
	int m_owner;
	bool facedown;
};

#endif