#include "BlackJack.h"

BlackJack::BlackJack(void)
{
	m_GameStatus = true;
}

BlackJack::~BlackJack(void)
{
}

int BlackJack::GameWinCheck()
{
	if(m_dealer.GetHandValue() < 17)
		return 0;

		int value = Minimum(m_dealer.GetHandValue(),m_player.GetHandValue());

		if(value == m_dealer.GetHandValue())
			return 1;
		else
			return 2;
}

int BlackJack::Minimum(int DealerHandValue, int PlayerHandValue)
{

	if((DealerHandValue - 21) > (PlayerHandValue - 21))
		return DealerHandValue;
	else
		return PlayerHandValue;
}
