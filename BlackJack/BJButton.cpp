#include <windows.h>
#include <string>
#include <gl\gl.h>
#include <gl\glu.h>

#include "TargaImage.h"
#include "BJButton.h"

using namespace std;

BJButton::BJButton(void)
{
	m_visibility = false;
	m_buttonWidth = 0;
	m_buttonHeight = 0;
	m_buttonName = "";
	m_buttonTexture = 0;
	//lpfnButtonHandler = NULL;

}

BJButton::~BJButton(void)
{
}

void BJButton::SetHeight(int height)
{
	m_buttonHeight = height;
}

void BJButton::SetWidth(int width)
{
	m_buttonWidth = width;
}

void BJButton::SetName(string name)
{
	m_buttonName = name;
}

void BJButton::SetEventFunction(void (*func)(void))
{
	lpfnButtonHandler = func;
}

void BJButton::SetPixelX(float x)
{
	m_pixelX = x;
}

void BJButton::SetPixelY(float y)
{
	m_pixelY = y;
}

void BJButton::SetVisibility(bool visible)
{
	m_visibility = visible;
}
bool BJButton::isVisible()
{
	if(m_visibility == false)
		return false;
	else return true;
}


void BJButton::RenderButton()
{
 glBindTexture(GL_TEXTURE_2D,m_buttonTexture);
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f,0.0f); glVertex2f(m_pixelX,m_pixelY);
		glTexCoord2f(1.0f,0.0f); glVertex2f(m_pixelX + 100,m_pixelY);
		glTexCoord2f(1.0f,1.0f); glVertex2f(m_pixelX + 100,m_pixelY + 100);
		glTexCoord2f(0.0f,1.0f); glVertex2f(m_pixelX,m_pixelY + 100);
	glEnd();

}

void BJButton::LoadButtonTexture()
{
	string filename;
	const char* temp = NULL;

	filename = m_buttonName;
	filename.append(".tga");
	temp = filename.c_str();

	CTargaImage image;

	image.LoadImageFile(temp);

	m_buttonWidth = image.GetWidth();
	m_buttonHeight = image.GetHeight();
	glGenTextures(1,&m_buttonTexture);
	glBindTexture(GL_TEXTURE_2D,m_buttonTexture);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();
	
}

void BJButton::DeleteTexture()
{
	glDeleteTextures(1,&m_buttonTexture);
}

