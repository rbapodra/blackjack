#pragma once
#include "FontFace.h"
#include "Glyph.h"
#include "tinyxml.h"
#include "glpng.h"

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>


class TextureFont
{
	FontFace Face;
	int pt;
	GLuint TexID;
	GLuint vbo[128];
	int TexWidth;
	int TexHeight;
	GLubyte *TexMemory;
	Glyph *TexGlyphs[256];
	void InitTexture();
	void InitVBOExtensions();
	void CreateVBO(int ch);
	void DrawVBO(int ch);
	GLubyte *PrepareGlyph(FT_Bitmap bitmap,int width,int height);
public:
	TextureFont(void);
	TextureFont(char *fontlocation,int pointsize);
	~TextureFont(void);
	int LoadFont(char* fontlocation,int pointsize);
	int LoadBMFont(char* fontlocation); 
	void RenderString(float x, float y, char * string);
	void RenderString(float x, float y, float width, float height, char *string);
};
