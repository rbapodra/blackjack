#include "Dealer.h"


Dealer::Dealer(void)
{
	m_Hand.reserve(2);
	m_handValue = 0;
	m_dealerAceCount = 0;
}

Dealer::~Dealer(void)
{
	
}

void Dealer::Hit(CardStack *pCardDeck)
{
	while(1)
	{
	  pCardDeck->DrawCard(NULL,this);
	  if(m_handValue >= 17)
		  break;
	}
}

void Dealer::SetHandValue(int value)
{
	m_handValue += value;
}

bool Dealer::isBusted()
{
	if(m_handValue > 21)
		return true;
	else return false;
}

bool Dealer::TestForBlackJack()
{
	if(m_handValue == 21 && (m_Hand.at(0).isAce() || m_Hand.at(1).isAce()) )	// keep a flag also because 21 has to occur in the 1st 2 cards only
	{	
			return true;
	}
	else return false;
}

void Dealer::Reset()
{
	m_Hand.clear();
	m_handValue = 0;
	m_dealerAceCount = 0;
}