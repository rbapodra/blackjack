//////// Main cpp file of the Black Jack game

#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <math.h>

#include "timer.h"
#include "BlackJack.h"
#include "GfxOpenGL.h"
//#include "BJButton.h"

LRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam);

void OpenGLSetup(HDC hDC);
void ButtonHandler(int x,int y,BlackJack* pMainGame);
void Reset();

// global pointer to the CGFXOpenGL class
CGfxOpenGL* g_glRender = NULL;
CHiResTimer* g_hiResTimer = NULL;
BlackJack mainGame;
 HDC hDC;
int mouseX,mouseY;
int choices;
int winStatus = 0;


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow)
{	  	
  // Define the main window class
	WNDCLASSEX wcex;
	HWND hwnd;


	MSG msg;
	bool quit = false;
	bool bFullScreen = false; // if the mode is fullscreen
		
	g_glRender = new CGfxOpenGL;
	g_hiResTimer = new CHiResTimer;

	RECT windowRect;
	DWORD dwExStyle;
	DWORD dwStyle;
	
	windowRect.left = (long)0;
	windowRect.right = (long)1024;
	windowRect.top = (long)0;
	windowRect.bottom = (long)768;

	wcex.cbSize			=	sizeof(WNDCLASSEX) ;
	wcex.style			=   CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	wcex.cbClsExtra		=   0 ;
	wcex.cbWndExtra		=   0 ;
	wcex.lpfnWndProc	=   WndProc ;
	wcex.hInstance		=   hInstance ;
	wcex.hIcon			=   LoadIcon(NULL,IDI_APPLICATION);
	wcex.hCursor		=   LoadCursor(NULL,IDC_ARROW) ;
	wcex.hbrBackground	=	NULL;		
	wcex.hIconSm		=   LoadIcon(NULL,IDI_APPLICATION);
	wcex.lpszMenuName	=	NULL ;
	wcex.lpszClassName	=	L"BlackJackGame" ;		// remove casting in visual c++ 6
	
	if(!RegisterClassEx(&wcex))
		return 0;

		// Once window is registered, before displaying check if fullscreen is enabled or not

	if(bFullScreen)
	{
		DEVMODE dmScreenSettings;	//device mode structure variable

		memset(&dmScreenSettings,0,sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = g_glRender->GetWindowWidth();
		dmScreenSettings.dmPelsHeight = g_glRender->GetWindowHeight();
		dmScreenSettings.dmBitsPerPel = g_glRender->GetWindowBits();
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		if(ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL){

			//Setting display mode failed, switch to windowed
			MessageBox(NULL,L"Display Mode Failed",NULL,MB_OK);	//remove casting in VC++ 6
			bFullScreen = false;
		}
	}

	if(bFullScreen)	// are we still in fullscreen mode 
	{
	
		dwExStyle = WS_EX_APPWINDOW; //window extended style
		dwStyle = WS_POPUP;
		ShowCursor(FALSE);
	}
	else
	{
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;	
		dwStyle = WS_OVERLAPPEDWINDOW;
	}


	//dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;

	AdjustWindowRectEx(&windowRect,dwStyle,FALSE,dwExStyle);
	// Create the Window

	hwnd = CreateWindowEx(0,
						  L"BlackJackGame",
						  L"Black Jack",
						  dwStyle |
						  WS_CLIPCHILDREN |
						  WS_CLIPSIBLINGS,
						  0,0,	// checkpoint
						  windowRect.right - windowRect.left,
						  windowRect.bottom - windowRect.top,
						  NULL,
						  NULL,
						  hInstance,
						  NULL
						  );

	hDC = GetDC(hwnd);

	if(hwnd == NULL)
		return 0;
	
		
	ShowWindow(hwnd,SW_SHOW);
	UpdateWindow(hwnd);

	if(!g_glRender->Init())
	{
		MessageBox(NULL,L"CGfxOpenGL::Init() error!",L"CGfxOpenGL class failed to initialize!", MB_OK);
		return -1;
	}

	

	g_hiResTimer->Init();
 
	choices = mainGame.m_player.GetChoicesLeft();
	
while((mainGame.GetGameStatus() != false) || (choices > 0))
{	
	//g_glRender->Prepare(g_hiResTimer->GetElapsedSeconds(1));

	g_glRender->m_Cards.Shuffle();
	
	if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
	{
		if(msg.message == WM_QUIT)
		{
			mainGame.SetGameStatus(false);
			break;
		}
		else
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	else
	{
	
		g_glRender->Render();
		SwapBuffers(hDC);
		
	}

    if(choices == 0)
	{	
		MessageBox(NULL,L"Game Over!!",L"Over",MB_OK);
		break;
	}

}
	
 delete g_hiResTimer;
 delete g_glRender;
	
   if(bFullScreen)
   {
	   ChangeDisplaySettings(NULL,0);
	   ShowCursor(TRUE);
   }

 ReleaseDC(hwnd,hDC);

 return msg.wParam;

}

LRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	static HDC hDC;
 	static HGLRC hRC;
	int width,height;

	switch(message)
	{
		case WM_CREATE:
			hDC = GetDC(hwnd);
			OpenGLSetup(hDC);
			hRC = wglCreateContext(hDC);	// Once pixel format is set up create the RC
			wglMakeCurrent(hDC,hRC);

			break;
		
		//case WM_QUIT:
		case WM_CLOSE:
		case WM_DESTROY:
			
			wglMakeCurrent(hDC,NULL);
			wglDeleteContext(hRC);
			PostQuitMessage(0);
			break;

		case WM_SIZE:
			height = HIWORD(lParam);
			width = LOWORD(lParam);
			g_glRender->SetupProjection(width,height);
			break;

		case WM_ACTIVATEAPP:
			break;

		case WM_KEYDOWN:
			int fwKeys;
			LPARAM keyData;

			fwKeys = (int)wParam;	// virtual key code
			keyData = lParam;

			switch(fwKeys)
			{
			case VK_ESCAPE:
				PostQuitMessage(0);
				break;
			default:
				break;
			}
			break;
		
		case WM_PAINT:				// paint
		PAINTSTRUCT ps;
		BeginPaint(hwnd, &ps);
		EndPaint(hwnd, &ps);
		break;

		case WM_LBUTTONDOWN:
			int oldmouseX,oldmouseY;

			oldmouseX = mouseX;
			oldmouseY = mouseY;

			mouseX = LOWORD(lParam);
			mouseY = HIWORD(lParam);
			ButtonHandler(mouseX,mouseY,&mainGame);

			break;
		
		case WM_RBUTTONDOWN:
			break;
		
		case WM_LBUTTONUP:
			break;

		case WM_RBUTTONUP:
			break;

		default:	return DefWindowProc(hwnd,message,wParam,lParam);
	}
	
	return 0;
}

void OpenGLSetup(HDC hDC)
{
	GLuint iPixelFormat;

	PIXELFORMATDESCRIPTOR pfd = 
	{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_SUPPORT_OPENGL | 
		PFD_DRAW_TO_WINDOW |
		PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		32,
		0,0,0,0,0,0,
		0,
		0,
		0,
		0,0,0,0,
		16,
		0,
		0,
		PFD_MAIN_PLANE,
		0,
		0,0,0
	};
	
	iPixelFormat = ChoosePixelFormat(hDC,&pfd); // int or GLUint doesnt matter
	SetPixelFormat(hDC,iPixelFormat,&pfd);
	
}

void ButtonHandler(int x,int y,BlackJack *pMainGame)
{
	//pMainGame->GetPlayer().OnHitPlayer(&pMainGame->GetCards());
	
	if( ((x >= 920) && (x <= 1020)) && ( y >= 628 && (y <= 728)))
	{
		g_glRender->quitbutton.lpfnButtonHandler();
	}

	if( (x >= 0 && x <= 100) && (y >= 628 && y <= 728))
	{
		if(g_glRender->dealbutton.isVisible())
			g_glRender->dealbutton.lpfnButtonHandler();
	}

	if( (x >= 0 && x <= 100) && ( y >= 468 && y <= 568))
	{
		if(g_glRender->hitbutton.isVisible())
		  g_glRender->hitbutton.lpfnButtonHandler();
	}

	if( (x >= 150 && x <= 250) && (y >= 468 && y <= 568))
	{
		if(g_glRender->standbutton.isVisible())
		   g_glRender->standbutton.lpfnButtonHandler();
	}

	if( (x >= 0 && x <= 100) && ( y >= 318 && y <= 418))
	{
		if(g_glRender->doubledownbutton.isVisible())
		   g_glRender->doubledownbutton.lpfnButtonHandler();
	}

	if( (x >= 700 && x <= 800) && ( y >= 518 && y <= 618))
	{
		if(g_glRender->okbutton.isVisible())
		   g_glRender->okbutton.lpfnButtonHandler();
	}

	if( (x >= 920 && x <= 1020) && ( y >= 518 && y <= 568))
	{
		mainGame.m_player.SetBetAmount(5);
		g_glRender->SetPlayerBetValue(mainGame.m_player.GetBetAmount());

		if(!g_glRender->dealbutton.isVisible())
			g_glRender->dealbutton.SetVisibility(true);
	}
	
	if( (x >= 920 && x <= 1020) && ( y >= 418 && y <= 468))
	{
		mainGame.m_player.SetBetAmount(25);
		g_glRender->SetPlayerBetValue(mainGame.m_player.GetBetAmount());

		if(!g_glRender->dealbutton.isVisible())
			g_glRender->dealbutton.SetVisibility(true);
	}

	if( (x >= 920 && x <= 1020) && ( y >= 318 && y <= 368))
	{
		mainGame.m_player.SetBetAmount(100);
		g_glRender->SetPlayerBetValue(mainGame.m_player.GetBetAmount());

		if(!g_glRender->dealbutton.isVisible())
			g_glRender->dealbutton.SetVisibility(true);
	}
	
	

}

void OnQuit()
{
	PostQuitMessage(0);
}

void OnDeal()
{
	g_glRender->dealbutton.SetVisibility(false);
	g_glRender->SetRenderChipsIndex(false);
	g_glRender->m_Cards.dealCards(&mainGame.m_player,&mainGame.m_dealer);
	g_glRender->hitbutton.SetVisibility(true);
	g_glRender->standbutton.SetVisibility(true);
	g_glRender->doubledownbutton.SetVisibility(true);
	g_glRender->SetPlayerHandValue(mainGame.m_player.GetHandValue());
	g_glRender->SetDealerHandValue(mainGame.m_dealer.GetHandValue());
	//check for blackjack
	if(mainGame.m_player.TestforBlackJack())
	{
		
		winStatus = 2;
		choices--;
		mainGame.m_player.SetBetAmount(mainGame.m_player.GetBetAmount()/2);
		mainGame.m_player.SetBalance(2);
		g_glRender->SetPlayerBalanceValue(mainGame.m_player.GetBalance());
		g_glRender->SetPlayerChancesValue(choices);
		g_glRender->SetRenderTextIndex(3);
		g_glRender->okbutton.SetVisibility(true);
		g_glRender->quitbutton.SetVisibility(false);
      	g_glRender->hitbutton.SetVisibility(false);
		g_glRender->standbutton.SetVisibility(false);
		g_glRender->doubledownbutton.SetVisibility(false);
		//MessageBox(NULL,L"You got Black Jack!! You win",L"Black Jack!!",MB_OK);
		//reset everything
		//Reset();
	}


}

void OnHit()
{
	g_glRender->doubledownbutton.SetVisibility(false);
	mainGame.m_player.OnHitPlayer(&(g_glRender->m_Cards));
	g_glRender->SetPlayerHandValue(mainGame.m_player.GetHandValue());
	// check for busted

	if(mainGame.m_player.isBusted())
			{
				//instead of message box display text and reset	
				
				choices--;
				winStatus = 1;
				// Reset everything
				//PostQuitMessage(0);
				mainGame.m_player.SetBalance(1);
				g_glRender->SetPlayerBalanceValue(mainGame.m_player.GetBalance());
				g_glRender->SetPlayerChancesValue(choices);
				g_glRender->SetRenderTextIndex(1);
				g_glRender->okbutton.SetVisibility(true);
				g_glRender->quitbutton.SetVisibility(false);
               	g_glRender->hitbutton.SetVisibility(false);
				g_glRender->standbutton.SetVisibility(false);
				g_glRender->doubledownbutton.SetVisibility(false);
				OnStand();
				//MessageBox(NULL,L"You Lose!!",L"Busted!!",MB_OK);
				//Reset();
			}
	
}

void OnStand()
{
	g_glRender->standbutton.SetVisibility(false);
	g_glRender->hitbutton.SetVisibility(false);
	mainGame.m_dealer.Hit(&(g_glRender->m_Cards));
	g_glRender->SetDealerHandValue(mainGame.m_dealer.GetHandValue());
	
	if(!mainGame.m_player.isBusted())
	{
		// check for dealer blackjack and busted or check for minimum
	if(mainGame.m_dealer.TestForBlackJack())
	{
		if(winStatus == 2) // means push
		{
			g_glRender->SetRenderTextIndex(7);
			g_glRender->okbutton.SetVisibility(true);
			g_glRender->quitbutton.SetVisibility(false);
			g_glRender->hitbutton.SetVisibility(false);
			g_glRender->standbutton.SetVisibility(false);
			g_glRender->doubledownbutton.SetVisibility(false);
			//MessageBox(NULL,L"Draw!! Split Cash",L"Push!",MB_OK);
		}
		else
		 {	
	    	 winStatus = 1;
			 mainGame.m_player.SetBalance(1);
			 g_glRender->SetPlayerBalanceValue(mainGame.m_player.GetBalance());
			 g_glRender->SetRenderTextIndex(4);
			 g_glRender->okbutton.SetVisibility(true);
			 g_glRender->quitbutton.SetVisibility(false);
			 g_glRender->hitbutton.SetVisibility(false);
			 g_glRender->standbutton.SetVisibility(false);
			 g_glRender->doubledownbutton.SetVisibility(false);
			  //MessageBox(NULL,L"Dealer has Black Jack!",L"You Lose",MB_OK);
		}
		choices--;

		//reset everything
	}
	if(mainGame.m_player.GetHandValue() == mainGame.m_dealer.GetHandValue())
	{
		// Push
		g_glRender->SetRenderTextIndex(7);	
		g_glRender->okbutton.SetVisibility(true);
		g_glRender->quitbutton.SetVisibility(false);
		g_glRender->hitbutton.SetVisibility(false);
		g_glRender->standbutton.SetVisibility(false);
		g_glRender->doubledownbutton.SetVisibility(false);
	}
	else if(mainGame.m_dealer.isBusted())
		{
			//instead of message box display text and reset
			choices--;
			winStatus = 2;
			mainGame.m_player.SetBalance(2);
			g_glRender->SetPlayerBalanceValue(mainGame.m_player.GetBalance());
			g_glRender->SetRenderTextIndex(2);
			g_glRender->okbutton.SetVisibility(true);
			g_glRender->quitbutton.SetVisibility(false);
			g_glRender->hitbutton.SetVisibility(false);
			g_glRender->standbutton.SetVisibility(false);
			g_glRender->doubledownbutton.SetVisibility(false);
			//MessageBox(NULL,L"Dealer Busted!!",L"Busted!!",MB_OK);
			// Reset everything
			//PostQuitMessage(0);
			
		}
	else 
	{
		winStatus = mainGame.GameWinCheck();
		if(winStatus != 0)
			choices--;
		if(winStatus == 1)
		{
			
			mainGame.m_player.SetBalance(1);
			g_glRender->SetPlayerBalanceValue(mainGame.m_player.GetBalance());
			g_glRender->SetRenderTextIndex(5);
			g_glRender->okbutton.SetVisibility(true);
			g_glRender->quitbutton.SetVisibility(false);
			g_glRender->hitbutton.SetVisibility(false);
			g_glRender->standbutton.SetVisibility(false);
			g_glRender->doubledownbutton.SetVisibility(false);
			//MessageBox(NULL,L"You lose",L"Game Over!",MB_OK);
		}
		if(winStatus == 2)
			{	
			
				mainGame.m_player.SetBalance(2);
				g_glRender->SetPlayerBalanceValue(mainGame.m_player.GetBalance());
				g_glRender->SetRenderTextIndex(6);
				g_glRender->okbutton.SetVisibility(true);
				g_glRender->quitbutton.SetVisibility(false);
				g_glRender->hitbutton.SetVisibility(false);
				g_glRender->standbutton.SetVisibility(false);
				g_glRender->doubledownbutton.SetVisibility(false);
				//MessageBox(NULL,L"You win",L"Congratulations!",MB_OK);
			}	
		//MessageBox(NULL,L"Dealer has standed",L"Dealer Standing!!",MB_OK);
	}
   }
	g_glRender->SetPlayerChancesValue(choices);
	//Reset();
}

void OnDoubleDown()
{
	g_glRender->doubledownbutton.SetVisibility(false);
	mainGame.m_player.SetBetAmount(mainGame.m_player.GetBetAmount());
	g_glRender->SetPlayerBetValue(mainGame.m_player.GetBetAmount());
	mainGame.m_player.OnDoubleDownPlayer(&(g_glRender->m_Cards));
	g_glRender->SetPlayerHandValue(mainGame.m_player.GetHandValue());
	g_glRender->SetDealerHandValue(mainGame.m_dealer.GetHandValue());

	if(mainGame.m_player.isBusted())
		  {
				//instead of message box display text and reset	
				choices--;
				winStatus = 1;
				mainGame.m_player.SetBalance(1);
		    	g_glRender->SetPlayerBalanceValue(mainGame.m_player.GetBalance());
				g_glRender->SetPlayerChancesValue(choices);
				g_glRender->SetRenderTextIndex(1);
				g_glRender->okbutton.SetVisibility(true);
				g_glRender->quitbutton.SetVisibility(false);
				g_glRender->hitbutton.SetVisibility(false);
				g_glRender->standbutton.SetVisibility(false);
				g_glRender->doubledownbutton.SetVisibility(false);
				OnStand();
				//MessageBox(NULL,L"You Lose!!",L"Busted!!",MB_OK);
				//Reset();
			}
	else
	 OnStand();

}

void Reset()
{
	g_glRender->dealbutton.SetVisibility(false);
	g_glRender->hitbutton.SetVisibility(false);
	g_glRender->standbutton.SetVisibility(false);
	g_glRender->doubledownbutton.SetVisibility(false);
	g_glRender->quitbutton.SetVisibility(true);
	g_glRender->okbutton.SetVisibility(false);
	mainGame.m_player.Reset();
	mainGame.m_dealer.Reset();
	g_glRender->m_Cards.Reset();
	g_glRender->SetDealerHandValue(0);
	g_glRender->SetPlayerHandValue(0);
	g_glRender->SetPlayerBetValue(0);
	g_glRender->SetRenderChipsIndex(true);
	g_glRender->SetRenderTextIndex(0);
}

void OnOk()
{
	Reset();
}