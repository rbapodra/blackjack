#pragma once

#include <string>

using namespace std;

class BJButton
{
public:
	BJButton(void);
	~BJButton(void);

	int GetWidth() {return m_buttonWidth; }
	int GetHeight() {return m_buttonHeight; }
	string GetName() {return m_buttonName; }
	float GetPixelX() {return m_pixelX; }
	float GetPixelY() {return m_pixelY; }
	GLuint GetButtonTexture() {return m_buttonTexture; }

	void SetWidth(int width);
	void SetHeight(int height);
	void SetName(string name);
	void SetEventFunction(void (*func)(void));
	void SetPixelX(float x);
	void SetPixelY(float y);

	bool isVisible();
	void RenderButton();
	bool GetVisibility() {return m_visibility; }
	void LoadButtonTexture();
	void SetVisibility(bool visible);
	void DeleteTexture();
	void (*lpfnButtonHandler)(void);

private:
	GLuint m_buttonTexture;
	int m_buttonWidth;
	int m_buttonHeight;
	string m_buttonName;
	bool m_visibility;
	float m_pixelX;
	float m_pixelY;

		
};

void OnQuit(void);
void OnDeal(void);
void OnHit(void);
void OnStand(void);
void OnDoubleDown(void);
void OnOk(void);