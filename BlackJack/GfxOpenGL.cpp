
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <string>
#include "glext.h"


#include "GfxOpenGL.h"
#include "TargaImage.h"
#include "BJButton.h"
#include "CardStack.h"
#include "TextureFont.h"

using namespace std;

extern HDC hDC;
TextureFont *Arial;

CGfxOpenGL::CGfxOpenGL(void)
{
	m_windowBits = 24;
	m_windowHeight = 768;
	m_windowWidth = 1024;
	m_angle = 0.0;
	playerhandValue = 0;
	dealerhandValue = 0;
	playerbalanceValue = 2000;
	playerbetamt = 0;
	playerchances = 10;

	for(int i = 0; i < 3; i++)
		m_chiptexture[i] = 0;

	renderchips = true;
}

CGfxOpenGL::~CGfxOpenGL(void)
{
}

bool CGfxOpenGL::Init()
{
	dealbutton.SetName("dealbutton");
	quitbutton.SetName("quitbutton");
	hitbutton.SetName("hitbutton");
	standbutton.SetName("standbutton");
	doubledownbutton.SetName("doubledownbutton");
	okbutton.SetName("okbutton");
	//okbutton.SetName("okbutton");

	dealbutton.SetVisibility(false);
	quitbutton.SetVisibility(true);
	hitbutton.SetVisibility(false);
	standbutton.SetVisibility(false);
	doubledownbutton.SetVisibility(false);
	okbutton.SetVisibility(false);
	//okbutton.SetVisibility(true);
	
	dealbutton.LoadButtonTexture();
	quitbutton.LoadButtonTexture();
	hitbutton.LoadButtonTexture();
	standbutton.LoadButtonTexture();
	doubledownbutton.LoadButtonTexture();
	//okbutton.LoadButtonTexture();
	okbutton.LoadButtonTexture();

	dealbutton.SetPixelX(0.0f);
	dealbutton.SetPixelY(40.0f);
	quitbutton.SetPixelX(920.0f);
	quitbutton.SetPixelY(40.0f);
	hitbutton.SetPixelX(0.0f);
	hitbutton.SetPixelY(200.0f);
	standbutton.SetPixelX(150.0f);
	standbutton.SetPixelY(200.0f);
	doubledownbutton.SetPixelX(0.0f);
	doubledownbutton.SetPixelY(350.0f);
	//okbutton.SetPixelX(0.0f);
	//okbutton.SetPixelX(40.0f);
	okbutton.SetPixelX(700.0f);
	okbutton.SetPixelY(150.0f);

	quitbutton.SetEventFunction(&OnQuit);	// this is called Registering the CallBack function with a particular event
	dealbutton.SetEventFunction(&OnDeal);
	hitbutton.SetEventFunction(&OnHit);
	standbutton.SetEventFunction(&OnStand);
	doubledownbutton.SetEventFunction(&OnDoubleDown);
	//okbutton.SetEventFunction(&OnOk);
	okbutton.SetEventFunction(&OnOk);
	
	LoadBettingChips();

	m_Cards.LoadCardTexture();
 
    // load texture font
	Arial = new TextureFont();
	if(!Arial->LoadFont("Fonts//Arial.ttf",16)){MessageBox(NULL,L"Load Font Error !!",L"CAUGHT AN EXCEPTION",MB_OK | MB_ICONINFORMATION);}
	

	m_fontListBase = CreateMyFont("Verdana", 48);
	m_textListBase = CreateMyFont("Verdana",15);
	

	glEnable(GL_TEXTURE_2D);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);	// enables Depth Testing
	

	return true;
}

bool CGfxOpenGL::ShutDown()
{
    quitbutton.DeleteTexture();
	dealbutton.DeleteTexture();
	hitbutton.DeleteTexture();
	standbutton.DeleteTexture();
	doubledownbutton.DeleteTexture();
	okbutton.DeleteTexture();
	//okbutton.DeleteTexture();
	ReleaseFont(m_fontListBase);
	ReleaseFont(m_textListBase);

	return true;
}

void CGfxOpenGL::SetupProjection(int width,int height)
{
	if(height == 0)
	{
		height = 1;
	}

	glViewport(0,0,width,height); // reset the viewport

	// Select the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	// reset the matrix
	glLoadIdentity();
	
	gluOrtho2D(0.0,m_windowWidth,0.0,m_windowHeight);

	// set the Modelview matrix to load the objects
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	m_windowHeight = height;
	m_windowWidth = width;

}

void CGfxOpenGL::Render()
{
	int index = 0;

	glClearColor(0.0f,0.0f,0.0f,0.0f);
	// clear the screen and the depth buffer 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	
	// Render the name of the game 
	//glColor3f(0.0f,0.0f,1.0f);
	//glColor3f(1.0,1.0,0.2);
	//Arial->RenderString(200,300,"BLACK JACK");
	RenderFont(400,700,m_fontListBase,"BLACK JACK");

	//glColor3f(1.0f,1.0f,1.0f);
	RenderBettingChips();
	//RenderFont(100,668,m_fontListBase,"OpenGL");

	if(dealbutton.isVisible())
		dealbutton.RenderButton();
	
	if(quitbutton.isVisible())
		quitbutton.RenderButton();

	if(hitbutton.isVisible())
		hitbutton.RenderButton();

	if(standbutton.isVisible())
		standbutton.RenderButton();

	if(doubledownbutton.isVisible())
		doubledownbutton.RenderButton();
	
	if(okbutton.isVisible())
		okbutton.RenderButton();
//	if(okbutton.isVisible())
//		okbutton.RenderButton();
	
	for(int i = 0; i < 52; i++)
	{
		if(m_Cards.CheckVisibility(i,index))
		{
		    m_Cards.RenderCard(index);
		}
	}
	
	if(m_Cards.GetFaceDownIndex() != -1)
		m_Cards.RenderCard(52);
	

	//render the names and hand values
	//glColor3f(0.0f,0.0f,1.0f);
	RenderFont(350,200,m_textListBase,"PLAYER HAND VALUE: ");
	RenderFont(350,450,m_textListBase,"DEALER HAND VALUE: ");
	RenderFont(350,150,m_textListBase,"Player BET AMOUNT: ");
	RenderFont(350,50,m_textListBase,"PLAYER BALANCE: ");
	RenderFont(350,10,m_textListBase,"CHANCES LEFT: ");
	RenderHandValue();

	RenderTextMessage();
	//glColor3f(1.0f,1.0f,1.0f);	
}

void CGfxOpenGL::Prepare(float dt)
{
	//m_angle += 5.0f;
}

void CGfxOpenGL::LoadBettingChips()
{
	CTargaImage image;

	image.LoadImageFile("$5.tga");
	glGenTextures(1,&m_chiptexture[0]);
	glBindTexture(GL_TEXTURE_2D,m_chiptexture[0]);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("$25.tga");
	glGenTextures(1,&m_chiptexture[1]);
	glBindTexture(GL_TEXTURE_2D,m_chiptexture[1]);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("$100.tga");
	glGenTextures(1,&m_chiptexture[2]);
	glBindTexture(GL_TEXTURE_2D,m_chiptexture[2]);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

}

void CGfxOpenGL::RenderBettingChips()
{
	if(renderchips == true)
	{
		glBindTexture(GL_TEXTURE_2D,m_chiptexture[0]);
		glBegin(GL_QUADS);
			glTexCoord2f(0.0f,0.0f); glVertex2f(920,200);
			glTexCoord2f(1.0f,0.0f); glVertex2f(970,200);
			glTexCoord2f(1.0f,1.0f); glVertex2f(970,250);
			glTexCoord2f(0.0f,1.0f); glVertex2f(920,250);
		glEnd();

		glBindTexture(GL_TEXTURE_2D,m_chiptexture[1]);
		glBegin(GL_QUADS);
			glTexCoord2f(0.0f,0.0f); glVertex2f(920,300);
			glTexCoord2f(1.0f,0.0f); glVertex2f(970,300);
			glTexCoord2f(1.0f,1.0f); glVertex2f(970,350);
			glTexCoord2f(0.0f,1.0f); glVertex2f(920,350);
		glEnd();
		
		glBindTexture(GL_TEXTURE_2D,m_chiptexture[2]);
		glBegin(GL_QUADS);
			glTexCoord2f(0.0f,0.0f); glVertex2f(920,400);
			glTexCoord2f(1.0f,0.0f); glVertex2f(970,400);
			glTexCoord2f(1.0f,1.0f); glVertex2f(970,450);
			glTexCoord2f(0.0f,1.0f); glVertex2f(920,450);
		glEnd();

	}
}
unsigned int CGfxOpenGL::CreateMyFont(char *fontName, int fontSize)
{
	HFONT hFont; // windows font
	unsigned int base;

	base = glGenLists(96);

	// include font type checking also

	hFont = CreateFont(fontSize,
							  0,
							  0,
							  0,
						FW_BOLD,
						  false,
						  false,
						  false,
						  ANSI_CHARSET,
						  OUT_TT_PRECIS,
						  CLIP_DEFAULT_PRECIS,
						  ANTIALIASED_QUALITY,
						  FF_DONTCARE | DEFAULT_PITCH,
						  (LPCWSTR)fontName);

	if(!hFont)
		return 0;

	SelectObject(hDC,hFont);
	wglUseFontBitmaps(hDC,32,96,base);

	return base;
}

void CGfxOpenGL::ReleaseFont(unsigned int base)
{
	if(base != 0)
		glDeleteLists(base,96);
}

void CGfxOpenGL::RenderFont(int xPos, int yPos, unsigned int base, char *str)
{

	if((base == 0)|| (!str))
		return;
	
	glRasterPos2i(xPos,yPos);

	glPushAttrib(GL_LIST_BIT);
		glListBase(base - 32);
		glCallLists((int)strlen(str),GL_UNSIGNED_BYTE,str);
	glPopAttrib();
	//Arial->RenderString(-0.55f,0.280f,"OpenGL FontAPI");
}

void CGfxOpenGL::RenderTextMessage()
{
	if(textindex == 1)
	{
		RenderFont(700,100,m_textListBase,"Busted!!!");
	}
	else if(textindex == 2)
	{
		RenderFont(700,100,m_textListBase,"Dealer Busted!!!");		
	}
	else if(textindex == 3)
	{
		RenderFont(700,100,m_textListBase,"You have BlackJack. You Win!!!");
	}
	else if(textindex == 4)
	{
		RenderFont(700,100,m_textListBase,"Dealer has BlackJack. You Lose!!!");
	}
	else if(textindex == 5)
	{
		RenderFont(700,100,m_textListBase,"You Lose!!!");
	}
	else  if(textindex == 6)
	{
		RenderFont(700,100,m_textListBase,"You Win! Congratulations!!!");
	}
	else if(textindex == 7)
	{
		RenderFont(700,100,m_textListBase,"Push!!");
	}
}

void CGfxOpenGL::RenderHandValue()
{
	char str[10];

	itoa(playerhandValue,str,10);
	
	RenderFont(500,200,m_textListBase,str);

	itoa(dealerhandValue,str,10);
	RenderFont(500,450,m_textListBase,str);

	itoa(playerbalanceValue,str,10);
	RenderFont(500,50,m_textListBase,str);

	itoa(playerbetamt,str,10);
	RenderFont(500,150,m_textListBase,str);

	itoa(playerchances,str,10);
	RenderFont(500,10,m_textListBase,str);
}