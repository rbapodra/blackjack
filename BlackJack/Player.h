/// declaration for the Player object

#pragma once

#ifndef PLAYER_H
#define PLAYER_H

#include "Card.h"
#include "CardStack.h"

#include <vector>

using namespace std;

class Card;
class CardStack;

class Player
{
public:

	bool TestforBlackJack();
	vector<Card> GetHand() { return m_Hand; }
	void OnHitPlayer(CardStack* pCardDeck);
	void OnDoubleDownPlayer(CardStack* pCardDeck);
	int GetChoicesLeft() {return m_ChoicesLeft; }
	int GetHandValue() {return m_handValue; }
	void SetBetAmount(int amount);
	int GetBetAmount() {return m_AmountBet;}
	int GetBalance() {return m_Balance;}
	void SetHandValue(int value);
	bool isBusted();
	void Reset();
	int GetAceCount() {return m_playerAceCount; }
	void SetAceCount() {m_playerAceCount++;}
	void SetBalance(int index);
	Player();
	~Player();

private:
	int m_Balance;
	int m_AmountBet;
	int m_ChoicesLeft;
	int m_handValue;
	int m_playerAceCount;

	vector<Card> m_Hand;
};

#endif