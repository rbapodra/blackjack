#pragma once


enum TargaFileType
{
	TGA_NODATA = 0,
	TGA_COLORMAPPED = 1,
	TGA_RGB = 2,
	TGA_GRAYSCALE = 3,
	TGA_COLORMAPPED_RLE = 9,
	TGA_RGB_RLE = 10,
	TGA_GRAYSCALE_RLE = 11
};

// Image Data Formats
#define	IMAGE_RGB       0
#define IMAGE_RGBA      1
#define IMAGE_LUMINANCE 2

// Image data types
#define	IMAGE_DATA_UNSIGNED_BYTE 0

// Pixel data transfer from file to screen:
// These masks are AND'd with the imageDesc in the TGA header,
// bit 4 is left-to-right ordering
// bit 5 is top-to-bottom
#define BOTTOM_LEFT  0x00	// first pixel is bottom left corner
#define BOTTOM_RIGHT 0x10	// first pixel is bottom right corner
#define TOP_LEFT     0x20	// first pixel is top left corner
#define TOP_RIGHT    0x30	// first pixel is top right corner

// tga file header
struct tgaheader_t
{
	unsigned char  idLength;		
	unsigned char  colorMapType;	
	unsigned char  imageTypeCode;	
	unsigned char  colorMapSpec[5];	
	unsigned short xOrigin;			
	unsigned short yOrigin;			
	unsigned short width;			
	unsigned short height;			
	unsigned char  bpp;				
	unsigned char  imageDesc;
};

// rgb structure
struct rgb_t
{
	unsigned char r;
	unsigned char g;
	unsigned char b;
};

// rgba structure
struct rgba_t
{
	unsigned char r;
	unsigned char g;
	unsigned char b;
	unsigned char a;
};

class CTargaImage
{
private:
	unsigned short m_Width;
	unsigned short m_Height;
	unsigned long m_ImageSize;
	unsigned char* m_pImageData;
	unsigned char m_ColorDepth;
	unsigned char m_ImageDataType;
	unsigned char m_ImageDataFormat;
	
	// we have to interchange the red and blue components
	void SwapRedBlue();

public:
	CTargaImage(void);
	virtual ~CTargaImage(void);

	// loading a targa image
	bool LoadImageFile(const char* filename);
	// unloading the image
	void Release();

	//to flip the image 
	bool FlipImage();	//flips the image vertically
	unsigned short GetWidth(){return m_Width;}
	unsigned short GetHeight(){return m_Height;}
	unsigned char GetImageDataFormat(){return m_ImageDataFormat;}
	unsigned char* GetImage(){return m_pImageData;}

	// Convert RGBA to RGBA and vice versa
	bool ConvertRGBAToRGB();
	bool ConvertRGBToRGBA(unsigned char alphaValue);
};
	