
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <time.h>
#include <ctime>


#include "Player.h"
#include "Dealer.h"

#include "TargaImage.h"
#include "CardStack.h"

int cardcount = 0;

CardStack::CardStack(void)
{

	m_CardDeck.reserve(53);

	for(int i = 0; i < 52; i++)
	{	
		Card C;
		C.SetSuit(clubs);
		C.SetValue(1);
		C.SetVisibility(false);
		C.SetOffset(0);
		C.SetOwner(0);
		pCardDeckIndices[i] = i;

		m_CardDeck.push_back(C);
	}

	Card C;
	C.SetSuit(clubs);
	C.SetValue(0);
	C.SetVisibility(false);
	C.SetOffset(1);
	C.SetOwner(0);
	m_CardDeck.push_back(C);
	
	for(int i = 0; i < 53; i++)
      m_cardtexture[i] = 0;
	
	top = 51;
	facedownindex = -1;	

	Initialize();
}

CardStack::~CardStack(void)
{
	m_CardDeck.clear();
    for(int i = 0; i < 53; i++)
	glDeleteTextures(1,&m_cardtexture[i]);
}

void CardStack::Initialize()
{
	unsigned int i;

	for(i = 0; i < 13; i++)
	{
		m_CardDeck.at(pCardDeckIndices[i]).SetSuit(clubs);
		if(i == 0)
			m_CardDeck.at(pCardDeckIndices[i]).SetValue(11);
		else if(i == 10 || i == 11 || i == 12)
			m_CardDeck.at(pCardDeckIndices[i]).SetValue(10);
		else
			m_CardDeck.at(pCardDeckIndices[i]).SetValue(i+1);
	}
	
	for(i = 13; i < 26; i++)
	{
		m_CardDeck.at(pCardDeckIndices[i]).SetSuit(diamonds);
		if(i == 13)
			m_CardDeck.at(pCardDeckIndices[i]).SetValue(11);
		else if(i == 23 || i == 24 || i == 25)
			m_CardDeck.at(pCardDeckIndices[i]).SetValue(10);
		else
			m_CardDeck.at(pCardDeckIndices[i]).SetValue(i-12);
	}

	for(i = 26; i < 39; i++)
	{
		m_CardDeck.at(pCardDeckIndices[i]).SetSuit(hearts);
		if(i == 26)
			m_CardDeck.at(pCardDeckIndices[i]).SetValue(11);
		else if(i == 36 || i == 37 || i == 38)
			m_CardDeck.at(pCardDeckIndices[i]).SetValue(10);
		else
			m_CardDeck.at(pCardDeckIndices[i]).SetValue(i-25);
	}

	for(i = 39; i < 52; i++)
	{
		m_CardDeck.at(pCardDeckIndices[i]).SetSuit(clubs);
		if(i == 39)
			m_CardDeck.at(pCardDeckIndices[i]).SetValue(11);
		else if(i == 49 || i == 50 || i == 51)
			m_CardDeck.at(pCardDeckIndices[i]).SetValue(10);
		else
			m_CardDeck.at(pCardDeckIndices[i]).SetValue(i-38);
	}


}
void CardStack::Shuffle()
{
	srand(time(0));
	
	// do the shuffling random number of times - fixed value say 500
	for(int i = 1; i <= 500; i++)
	{
		int index1 = rand() % 52; 
		int index2 = rand() % 52;
		int temp = pCardDeckIndices[index1];
		pCardDeckIndices[index1] = pCardDeckIndices[index2];
		pCardDeckIndices[index2] = temp;
	}
 
}

void CardStack::dealCards(Player* pPlayer,Dealer* pDealer)
{
	// dealing 2 cards
	//since it is the first card, assume ACE to be equal to 11
	pPlayer->GetHand().push_back(m_CardDeck.at(pCardDeckIndices[top]));
	pPlayer->SetHandValue(m_CardDeck.at(pCardDeckIndices[top]).GetValue());
	if(m_CardDeck.at(pCardDeckIndices[top]).isAce())
		pPlayer->SetAceCount();
	m_CardDeck.at(pCardDeckIndices[top]).SetVisibility(true);
	m_CardDeck.at(pCardDeckIndices[top]).SetOffset(51 - top);
	m_CardDeck.at(pCardDeckIndices[top]).SetOwner(1);
	top--;

	pDealer->GetHand().push_back(m_CardDeck.at(pCardDeckIndices[top]));
	m_CardDeck.at(pCardDeckIndices[top]).SetVisibility(true);
	m_CardDeck.at(pCardDeckIndices[top]).SetOffset(51 - (top + 1));
	m_CardDeck.at(pCardDeckIndices[top]).SetOwner(0);
	pDealer->SetHandValue(m_CardDeck.at(pCardDeckIndices[top]).GetValue());
	if(m_CardDeck.at(pCardDeckIndices[top]).isAce())
		pDealer->SetAceCount();
	top--;
	
	pPlayer->GetHand().push_back(m_CardDeck.at(pCardDeckIndices[top]));
	m_CardDeck.at(pCardDeckIndices[top]).SetVisibility(true);
	m_CardDeck.at(pCardDeckIndices[top]).SetOffset(51 - (top + 1));
	m_CardDeck.at(pCardDeckIndices[top]).SetOwner(1);
	if(m_CardDeck.at(pCardDeckIndices[top]).isAce() && m_CardDeck.at(pCardDeckIndices[top-2]).isAce())
	{
		pPlayer->SetHandValue(1);
		pPlayer->SetAceCount();
	}
	else
		pPlayer->SetHandValue(m_CardDeck.at(pCardDeckIndices[top]).GetValue());

	top--;

	pDealer->GetHand().push_back(m_CardDeck.at(pCardDeckIndices[top]));
	m_CardDeck.at(52).SetVisibility(true);
	facedownindex = pCardDeckIndices[top];
	m_CardDeck.at(pCardDeckIndices[top]).FaceDown(true);
	m_CardDeck.at(pCardDeckIndices[top]).SetOffset(51 - (top + 2));
	m_CardDeck.at(pCardDeckIndices[top]).SetOwner(0);
	m_CardDeck.at(52).SetOffset(51 - (top + 2)); 
	m_CardDeck.at(52).SetOwner(0);
	top--;

}

void CardStack::DrawCard(Player* pPlayer,Dealer* pDealer)
{
	
	if(pPlayer != NULL && pDealer == NULL)
	{
		pPlayer->GetHand().push_back(m_CardDeck.at(pCardDeckIndices[top]));
		m_CardDeck.at(pCardDeckIndices[top]).SetVisibility(true);
		m_CardDeck.at(pCardDeckIndices[top]).SetOffset(49 - top );
		cardcount++;
		m_CardDeck.at(pCardDeckIndices[top]).SetOwner(1);
		
		if(m_CardDeck.at(pCardDeckIndices[top]).isAce() && (pPlayer->GetHandValue() + (m_CardDeck.at(pCardDeckIndices[top]).GetValue()) > 21))
		{
			pPlayer->SetHandValue(1);
			pPlayer->SetAceCount();
		}
		else if(!(m_CardDeck.at(pCardDeckIndices[top]).isAce()) && ((pPlayer->GetHandValue() + (m_CardDeck.at(pCardDeckIndices[top]).GetValue()) > 21) && (pPlayer->GetAceCount() != 0)))
		{
			pPlayer->SetHandValue(pPlayer->GetHandValue() - 11 + 1 + m_CardDeck.at(pCardDeckIndices[top]).GetValue());	
		}
		else
		{
			pPlayer->SetHandValue(m_CardDeck.at(pCardDeckIndices[top]).GetValue());
		}

		top--;

	}
	else if(pDealer != NULL && pPlayer == NULL)
	{
		if(m_CardDeck.at(52).isVisible())
		{
			m_CardDeck.at(52).SetVisibility(false);
			m_CardDeck.at(facedownindex).SetVisibility(true);
			pDealer->SetHandValue(m_CardDeck.at(facedownindex).GetValue());

			if(m_CardDeck.at(50).isAce() && m_CardDeck.at(48).isAce())
			{	
				pDealer->SetHandValue(1);
				pDealer->SetAceCount();
			}
			
		}
		else
		{
			 pDealer->GetHand().push_back(m_CardDeck.at(pCardDeckIndices[top]));
		     m_CardDeck.at(pCardDeckIndices[top]).SetVisibility(true);
		     m_CardDeck.at(pCardDeckIndices[top]).SetOffset((49 - (top + cardcount)));
		     m_CardDeck.at(pCardDeckIndices[top]).SetOwner(0);

		     if(m_CardDeck.at(pCardDeckIndices[top]).isAce() && (pDealer->GetHandValue() + (m_CardDeck.at(pCardDeckIndices[top]).GetValue()) > 21))
			 {
				pDealer->SetHandValue(1);
				pDealer->SetAceCount();
			 }
		else if(!(m_CardDeck.at(pCardDeckIndices[top]).isAce()) && ((pDealer->GetHandValue() + (m_CardDeck.at(pCardDeckIndices[top]).GetValue()) > 21) && (pDealer->GetAceCount() != 0)))
		{
			pDealer->SetHandValue(pDealer->GetHandValue() - 11 + 1 + m_CardDeck.at(pCardDeckIndices[top]).GetValue());	
		}
		else
		{
			pDealer->SetHandValue(m_CardDeck.at(pCardDeckIndices[top]).GetValue());
		}	
		  top--;
		}
		
	}

}

bool CardStack::CheckVisibility(int index,int& position)
{
	if(m_CardDeck.at(pCardDeckIndices[index]).isVisible())
	{
		position = pCardDeckIndices[index];
		return true;
	}
	else return false;
}

void CardStack::SetCardVisibility(int index,bool visible)
{
	m_CardDeck.at(index).SetVisibility(visible);
}

bool CardStack::CheckFaceDown(int index)
{
	if(m_CardDeck.at(pCardDeckIndices[index]).isFaceDown())
		return true;
	else
		return false;
}
void CardStack::RenderCard(int index)
{
	if(m_CardDeck.at(index).GetOwner() == 1)
	{
		glBindTexture(GL_TEXTURE_2D,m_cardtexture[index]);
		glBegin(GL_QUADS);
		glTexCoord2f(0.0f,0.0f); glVertex2f(350.0f + (m_CardDeck.at(index).GetOffset() * 99) ,250.0f);
			glTexCoord2f(1.0f,0.0f); glVertex2f(450.0f + (m_CardDeck.at(index).GetOffset() * 99),250.0f);
			glTexCoord2f(1.0f,1.0f); glVertex2f(450.0f + (m_CardDeck.at(index).GetOffset() * 99),350.0f);
			glTexCoord2f(0.0f,1.0f); glVertex2f(350.0f + (m_CardDeck.at(index).GetOffset() * 99),350.0f);
		glEnd();
	}

	else if(m_CardDeck.at(index).GetOwner() == 0)
	{
		glBindTexture(GL_TEXTURE_2D,m_cardtexture[index]);
		glBegin(GL_QUADS);
		glTexCoord2f(0.0f,0.0f); glVertex2f(350.0f + (m_CardDeck.at(index).GetOffset() * 99) ,500.0f);
			glTexCoord2f(1.0f,0.0f); glVertex2f(450.0f + (m_CardDeck.at(index).GetOffset() * 99),500.0f);
			glTexCoord2f(1.0f,1.0f); glVertex2f(450.0f + (m_CardDeck.at(index).GetOffset() * 99),600.0f);
			glTexCoord2f(0.0f,1.0f); glVertex2f(350.0f + (m_CardDeck.at(index).GetOffset() * 99),600.0f);
		glEnd();
	}
   
}

void CardStack::Reset()
{
	top = 51;
	facedownindex = -1;	
	cardcount = 0;

	for(int i = 0; i < 52; i++)
	{
		m_CardDeck.at(i).SetOffset(0);
		m_CardDeck.at(i).SetOwner(0);
		m_CardDeck.at(i).SetVisibility(false);
	}

	m_CardDeck.at(52).SetOffset(1);
	m_CardDeck.at(52).SetOwner(0);
	m_CardDeck.at(52).SetVisibility(false);

}

void CardStack::LoadCardTexture()
{
	
	CTargaImage image;
	

	image.LoadImageFile("b1fv.tga");
	glGenTextures(1,&m_cardtexture[52]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[52]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);	
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("c1.tga");
	glGenTextures(1,&m_cardtexture[0]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[0]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("c2.tga");
	glGenTextures(1,&m_cardtexture[1]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[1]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();
	
	image.LoadImageFile("c3.tga");
	glGenTextures(1,&m_cardtexture[2]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[2]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("c4.tga");
	glGenTextures(1,&m_cardtexture[3]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[3]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("c5.tga");
	glGenTextures(1,&m_cardtexture[4]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[4]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("c6.tga");
	glGenTextures(1,&m_cardtexture[5]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[5]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("c7.tga");
	glGenTextures(1,&m_cardtexture[6]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[6]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("c8.tga");
	glGenTextures(1,&m_cardtexture[7]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[7]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("c9.tga");
	glGenTextures(1,&m_cardtexture[8]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[8]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("c10.tga");
	glGenTextures(1,&m_cardtexture[9]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[9]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("cj.tga");
	glGenTextures(1,&m_cardtexture[10]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[10]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("cq.tga");
	glGenTextures(1,&m_cardtexture[11]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[11]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("ck.tga");
	glGenTextures(1,&m_cardtexture[12]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[12]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("d1.tga");
	glGenTextures(1,&m_cardtexture[13]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[13]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("d2.tga");
	glGenTextures(1,&m_cardtexture[14]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[14]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();
	
	image.LoadImageFile("d3.tga");
	glGenTextures(1,&m_cardtexture[15]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[15]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("d4.tga");
	glGenTextures(1,&m_cardtexture[16]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[16]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("d5.tga");
	glGenTextures(1,&m_cardtexture[17]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[17]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("d6.tga");
	glGenTextures(1,&m_cardtexture[18]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[18]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("d7.tga");
	glGenTextures(1,&m_cardtexture[19]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[19]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("d8.tga");
	glGenTextures(1,&m_cardtexture[20]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[20]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("d9.tga");
	glGenTextures(1,&m_cardtexture[21]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[21]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("d10.tga");
	glGenTextures(1,&m_cardtexture[22]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[22]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("dj.tga");
	glGenTextures(1,&m_cardtexture[23]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[23]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("dq.tga");
	glGenTextures(1,&m_cardtexture[24]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[24]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("dk.tga");
	glGenTextures(1,&m_cardtexture[25]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[25]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("h1.tga");
	glGenTextures(1,&m_cardtexture[26]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[26]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("h2.tga");
	glGenTextures(1,&m_cardtexture[27]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[27]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();
	
	image.LoadImageFile("h3.tga");
	glGenTextures(1,&m_cardtexture[28]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[28]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("h4.tga");
	glGenTextures(1,&m_cardtexture[29]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[29]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("h5.tga");
	glGenTextures(1,&m_cardtexture[30]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[30]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("h6.tga");
	glGenTextures(1,&m_cardtexture[31]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[31]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("h7.tga");
	glGenTextures(1,&m_cardtexture[32]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[32]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("h8.tga");
	glGenTextures(1,&m_cardtexture[33]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[33]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("h9.tga");
	glGenTextures(1,&m_cardtexture[34]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[34]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("h10.tga");
	glGenTextures(1,&m_cardtexture[35]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[35]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("hj.tga");
	glGenTextures(1,&m_cardtexture[36]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[36]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("hq.tga");
	glGenTextures(1,&m_cardtexture[37]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[37]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("hk.tga");
	glGenTextures(1,&m_cardtexture[38]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[38]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("s1.tga");
	glGenTextures(1,&m_cardtexture[39]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[39]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("s2.tga");
	glGenTextures(1,&m_cardtexture[40]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[40]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();
	
	image.LoadImageFile("s3.tga");
	glGenTextures(1,&m_cardtexture[41]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[41]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("s4.tga");
	glGenTextures(1,&m_cardtexture[42]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[42]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("s5.tga");
	glGenTextures(1,&m_cardtexture[43]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[43]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("s6.tga");
	glGenTextures(1,&m_cardtexture[44]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[44]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("s7.tga");
	glGenTextures(1,&m_cardtexture[45]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[45]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("s8.tga");
	glGenTextures(1,&m_cardtexture[46]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[46]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("s9.tga");
	glGenTextures(1,&m_cardtexture[47]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[47]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("s10.tga");
	glGenTextures(1,&m_cardtexture[48]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[48]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("sj.tga");
	glGenTextures(1,&m_cardtexture[49]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[49]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("sq.tga");
	glGenTextures(1,&m_cardtexture[50]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[50]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

	image.LoadImageFile("sk.tga");
	glGenTextures(1,&m_cardtexture[51]);
	glBindTexture(GL_TEXTURE_2D,m_cardtexture[51]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGB,image.GetWidth(),image.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,image.GetImage());
	image.Release();

}

