#include "TextureFont.h"


typedef struct
{
	GLfloat location[3];
	GLfloat tex[2];

}vertex;

#define BUFFER_OFFSET(i) ((char*)NULL + (i))
#define GL_ARRAY_BUFFER_ARB 0x8892
#define GL_STATIC_DRAW_ARB 0x88E4

typedef void (APIENTRY * PFNGLBINDBUFFERARBPROC) (GLenum target, GLuint buffer);
typedef void (APIENTRY * PFNGLDELETEBUFFERSARBPROC) (GLsizei n, const GLuint *buffers);
typedef void (APIENTRY * PFNGLGENBUFFERSARBPROC) (GLsizei n, GLuint *buffers);
typedef void (APIENTRY * PFNGLBUFFERDATAARBPROC) (GLenum target, int size, const GLvoid *data, GLenum usage);

// VBO Extension Function Pointers
PFNGLGENBUFFERSARBPROC glGenBuffersARB = NULL;					// VBO Name Generation Procedure
PFNGLBINDBUFFERARBPROC glBindBufferARB = NULL;					// VBO Bind Procedure
PFNGLBUFFERDATAARBPROC glBufferDataARB = NULL;					// VBO Data Loading Procedure
PFNGLDELETEBUFFERSARBPROC glDeleteBuffersARB = NULL;			// VBO Deletion Procedure

int nextPowerOf2(int n)			
{
	int i = 1;
	while (i < n)
	{i*= 2;}
	return i;
}


void TextureFont::InitVBOExtensions()
{
 glGenBuffersARB = (PFNGLGENBUFFERSARBPROC) wglGetProcAddress("glGenBuffersARB");
 glBindBufferARB = (PFNGLBINDBUFFERARBPROC) wglGetProcAddress("glBindBufferARB");
 glBufferDataARB = (PFNGLBUFFERDATAARBPROC) wglGetProcAddress("glBufferDataARB");
 glDeleteBuffersARB = (PFNGLDELETEBUFFERSARBPROC) wglGetProcAddress("glDeleteBuffersARB");
}


TextureFont::TextureFont(void)
{
}

TextureFont::~TextureFont(void)
{
}
TextureFont::TextureFont(char *fontlocation,int pointsize)
{
 LoadFont(fontlocation,pointsize);
}

void TextureFont::InitTexture()
{
	TexMemory = new GLubyte[TexWidth*TexHeight*2];
	memset(TexMemory,0,TexWidth*TexHeight*2);

	glGenTextures(1,&TexID);								  //Generate a Texture with id -> TexID
	glBindTexture(GL_TEXTURE_2D,TexID);						  //Bind Texture with id -> TexID
    glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);//Set up Texture Enviornment Parameters

	  //Set up Texture Parameters	
	  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D,0,1,TexHeight,TexWidth,0,GL_LUMINANCE_ALPHA,GL_UNSIGNED_BYTE,TexMemory); //Initialize the Texture
}

GLubyte *TextureFont::PrepareGlyph(FT_Bitmap bitmap,int width,int height)
{
		int val=0,k=0;				
		GLubyte *glyphdata;
		glyphdata = new GLubyte[width*height*2];		
		
        for(int r=0;r<height;r++)
	   {for(int c=0;c<width;c++)
	   {	val=(c>=bitmap.width||r>=bitmap.rows)?0:bitmap.buffer[c+bitmap.width*r]; 			
			glyphdata[k*2]=val;
			glyphdata[k*2+1]=val;
			k++;
		}}
		
		return glyphdata;
}

int TextureFont::LoadFont(char *fontlocation,int pointsize)
{
	if(!Face.LoadFace(fontlocation,pointsize)){return FALSE;}  //Load Face from fontfile with reqd pointsize	
	pt=pointsize;TexWidth= pt*24; TexHeight= pt*24;
	int penX=0,penY=pt*2;
	InitTexture();						//Initialize Texture Memory and Texture Parameters
 
	for(int i=32;i<127;i++)               //Run through all valid characters
	{ 	
		FT_Bitmap bitmap = Face.LoadGlyphBitmap(i);    //Load Bitmap of Character with charcode 'i'

		int width = bitmap.width;width = nextPowerOf2(width);  //Get Width of bitmap glyph
		int height = bitmap.rows;height = nextPowerOf2(height); //Get Height of bitmap glyph
	
		if(penX+Face.getAdvance() >=(TexWidth-1))  //Check boundary conditions
		{penY=penY+Face.getAscent()+10;penX=0;}		//Go to next line
										
		if(penY+height>=TexHeight-1)	//Check boundary conditions
		return true;					//Max no of characters filled so end

		GLubyte *glyphdata = PrepareGlyph(bitmap,width,height);  //Convert glyph format from FT_Bitmap to GLubyte
		glTexSubImage2D(GL_TEXTURE_2D,0,penX  ,penY - Face.getHoriBearing(),width,height,GL_LUMINANCE_ALPHA,GL_UNSIGNED_BYTE,glyphdata);//Add glyph to texture
		delete[] glyphdata;
			
		TexGlyphs[i] = new Glyph(penX ,penY - Face.getHoriBearing(),bitmap.width,bitmap.rows,Face.getHoriBearing(),Face.getAdvance());//Create a glyph structure with information about the glyph
  					
		penX=penX+Face.getAdvance()+4;	//Advance the pen to draw next glyph
	             }


			InitVBOExtensions();
			for(int i=32;i<127;i++)
			CreateVBO(i);			//Create VBOs for all the characters in the texture
	

	return true;
}

int TextureFont::LoadBMFont(char *fontlocation)
{
	 TiXmlDocument XMLdoc(fontlocation);		
     bool loadOkay = XMLdoc.LoadFile();			//Load the XML doc of the font
	 if (loadOkay)
	 {
		TiXmlElement *pRoot, *pInfo, *pFile, *pChars, *pChar;
		pRoot = XMLdoc.FirstChildElement("font");
		if (pRoot)
		{ 
             pInfo = pRoot->FirstChildElement("info");
			 pt=atoi(pInfo->Attribute("size"));			//Point Size
			 pInfo = pInfo->NextSiblingElement("common");
			  
			 TexWidth=atoi(pInfo->Attribute("scaleW"));	//Texture Width
			 TexHeight=atoi(pInfo->Attribute("scaleH"));//Texture Height	
			 InitTexture();								//Initialize Texture

			 pInfo=pInfo->NextSiblingElement("pages");
			 pFile=pInfo->FirstChildElement("page");

			 const char* img;
			 img = pFile->Attribute("file");	//get the img file name

			pngInfo info;
			glBindTexture(GL_TEXTURE_2D,TexID);
         if (!pngLoad(img, PNG_NOMIPMAP, PNG_SOLID, &info)) {  //Load png file(the charmap) of the font as a texture           
					return false;
           }

			 pChars = pRoot->FirstChildElement("chars");
			 pChar = pChars->FirstChildElement("char");		
			int ch,XOff,YOff,Width,Height,HoriBearing,Advance;

		while(pChar)//Parse through all the characters in the charmap
		{   
			ch		=atoi(pChar->Attribute("id"));
			XOff	=atoi(pChar->Attribute("x"));
			YOff	=atoi(pChar->Attribute("y"));
			Width	=atoi(pChar->Attribute("width"));
			Height	=atoi(pChar->Attribute("height"));
			HoriBearing=-atoi(pChar->Attribute("yoffset"));
			Advance	=atoi(pChar->Attribute("xadvance"));
			TexGlyphs[ch] = new Glyph(XOff,YOff,Width,Height,HoriBearing,Advance);

			pChar = pChar->NextSiblingElement("char");				
		}
	   }
	 }
	InitVBOExtensions();
	for(int i=32;i<127;i++)
	CreateVBO(i);

  return true;
}
void TextureFont::RenderString(float x,float y,char *string)
{
    char *c;
	glBindTexture(GL_TEXTURE_2D,TexID); //Bind the Texture for getting characters
	glPushMatrix();
	glTranslatef(x,y,0.0f);
		for(c=string;*c;c++)//Parse through all the characters in the string
		{DrawVBO(*c);}	//Draw a texture mapped quad of the character
	glPopMatrix();	
}

void TextureFont::RenderString(float x, float y, float width, float height, char *string)
{
 float penx=0.0f,peny=0.0f;
 char *c;int ch;float div = 512.0;

 glBindTexture(GL_TEXTURE_2D,TexID);
 glPushMatrix();
    glTranslatef(x,y,0.0f);
     glPushMatrix();int k=1;
	for(c=string;*c;c++)
	{   
		ch=*c;
		penx+=(float)(TexGlyphs[ch]->getAdvance()/div);

		if(penx>width)
		{
		 glPopMatrix();
         glTranslatef(0.0,-pt/div,0.0f);
		 glPushMatrix();
		 penx=(float)(TexGlyphs[ch]->getAdvance()/div);peny+=0.05;
		 if(peny>height)break;
		}
		 DrawVBO(*c);
	 }
	glPopMatrix();
 glPopMatrix();
}

void TextureFont::CreateVBO(int ch)
{
		float RealXOff = (float)TexGlyphs[ch]->getXOffset()/(float)TexWidth;
		float RealYOff = (float)TexGlyphs[ch]->getYOffset()/(float)TexHeight;
		float RealHeight = (float)(TexGlyphs[ch]->getHeight())/(float)TexHeight;
		float RealWidth = (float)(TexGlyphs[ch]->getWidth())/(float)TexWidth;

		float div = 512.0;

	vertex verts[4];
	verts[0].location[0]=0.0;	verts[0].location[1]=-(float)(TexGlyphs[ch]->getHeight() - TexGlyphs[ch]->getHoriBearing())/div;	verts[0].location[2]=0.0;
	verts[0].tex[0]=RealXOff;	verts[0].tex[1]=RealYOff + RealHeight;

	verts[1].location[0]=0.0;	verts[1].location[1]=(float)TexGlyphs[ch]->getHoriBearing()/div;	verts[1].location[2]=0.0;
	verts[1].tex[0]=RealXOff;	verts[1].tex[1]=RealYOff;

	verts[2].location[0]=(float)(TexGlyphs[ch]->getWidth()/div);	verts[2].location[1]=(float) TexGlyphs[ch]->getHoriBearing()/div;	verts[2].location[2]=0.0;
	verts[2].tex[0]=RealXOff + RealWidth;verts[2].tex[1]=RealYOff;

	verts[3].location[0]=(float)(TexGlyphs[ch]->getWidth()/div);verts[3].location[1]=-(float)(TexGlyphs[ch]->getHeight() - TexGlyphs[ch]->getHoriBearing())/div;	verts[3].location[2]=0.0;
	verts[3].tex[0]= RealXOff + RealWidth;verts[3].tex[1]= RealYOff + RealHeight;


    glGenBuffersARB(1,&vbo[ch]);
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, vbo[ch]);
	glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(vertex)*4, verts, GL_STATIC_DRAW_ARB);
	glVertexPointer(3, GL_FLOAT, sizeof(vertex), BUFFER_OFFSET(0));
	glTexCoordPointer(2, GL_FLOAT, sizeof(vertex), BUFFER_OFFSET(12));

}
void TextureFont::DrawVBO(int ch)
{
glBindBufferARB(GL_ARRAY_BUFFER_ARB,vbo[ch]);

glEnableClientState(GL_TEXTURE_COORD_ARRAY);
glEnableClientState(GL_VERTEX_ARRAY);

glVertexPointer(3, GL_FLOAT, sizeof(vertex), BUFFER_OFFSET(0));
glTexCoordPointer(2, GL_FLOAT, sizeof(vertex), BUFFER_OFFSET(12));

glDrawArrays(GL_QUADS,0,4);


glDisableClientState(GL_TEXTURE_COORD_ARRAY);
glDisableClientState(GL_VERTEX_ARRAY);

glTranslatef((float)(TexGlyphs[ch]->getAdvance()/512.0),0,0);

}