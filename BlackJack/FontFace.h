#pragma once

#include "ft2build.h"
#include <freetype/freetype.h>
#include <freetype/ftglyph.h>
#include <freetype/ftoutln.h>
#include <freetype/fttrigon.h>


class FontFace
{
	FT_Library library;
	FT_Face face;
public:	
	FontFace();
   ~FontFace(void);
	int LoadFace(char* fontlocation,int pointsize);
	int getAscent();
	int getDescent();
	int getLineGap();
	int getHoriBearing();
	int getAdvance();
	FT_Bitmap LoadGlyphBitmap(int charcode);
};
