#include <fstream>
#include "TargaImage.h"

using namespace std;

CTargaImage::CTargaImage(void)
{
	m_pImageData = NULL;
}

CTargaImage::~CTargaImage(void)
{
	Release();
}

void CTargaImage::SwapRedBlue()
{
	switch(m_ColorDepth)
	{
	case 32:
		{
			unsigned char temp;
			rgba_t* src = (rgba_t*)m_pImageData;

			for(int pixel = 0; pixel < (m_Width * m_Height); pixel++)
			{
				temp = src[pixel].b;
				src[pixel].b = src[pixel].r;
				src[pixel].r = temp;
			}
		}
		break;

	case 24:
		{
			unsigned char temp;
			rgb_t* src = (rgb_t*)m_pImageData;

			for(int pixel = 0; pixel < (m_Width * m_Height); pixel++)
			{
				temp = src[pixel].b;
				src[pixel].b = src[pixel].r;
				src[pixel].r = temp;
			}
		}
			break;

	default:
		//ignore
		break;
	}
}

bool CTargaImage::LoadImageFile(const char *filename)
{
 fstream fptr;
 tgaheader_t tgaImageHeader;

  fptr.open(filename,ios::in | ios::binary);
 if(!fptr)
 {
	 return false;
 }

 // read the file image header
 fptr.read((char*)&tgaImageHeader,sizeof(tgaheader_t));

 // check if the image is of the type targa supports .. RGB,RGB_RLE,...
 if((tgaImageHeader.imageTypeCode != TGA_RGB) && (tgaImageHeader.imageTypeCode != TGA_RGB_RLE) &&
	 (tgaImageHeader.imageTypeCode != TGA_GRAYSCALE) && (tgaImageHeader.imageTypeCode != TGA_GRAYSCALE_RLE) ||
	 (tgaImageHeader.colorMapType != 0) )
 {
	 fptr.close();
	 //fclose(pFile);
	 return false;
 }

 // get the dimensions of the image
 m_Width = tgaImageHeader.width;
 m_Height = tgaImageHeader.height;

 // inorder to find the size of the image, we need color mode also
 // color mode = 3 -> BGR, color mode = 4 -> BGRA

 int colorMode = tgaImageHeader.bpp / 8;

 // we dont want to handle less than 24-bit cuz they wont b rgb or rgba type

 if(colorMode < 3)
 {
	 fptr.close();
	// fclose(pFile);
	 return false;
 }

 m_ImageSize = m_Width * m_Height * colorMode;

 // once we get the imagesize, we can allocate that much size to our imagedata variable
 m_pImageData = new unsigned char[m_ImageSize];

 // skip past the id field
if(tgaImageHeader.idLength > 0)
{
	fptr.seekg(tgaImageHeader.idLength,ios::cur);
	//fseek(pFile, SEEK_CUR, tgaImageHeader.idLength);

}

// if the image is uncompressed, then directly read the data
if(tgaImageHeader.imageTypeCode == TGA_RGB || tgaImageHeader.imageTypeCode == TGA_GRAYSCALE)
{
	fptr.read((char*)m_pImageData,m_ImageSize);
	//fread(m_pImageData, 1, m_ImageSize, pFile);
}
else
{
	// image is an RLE compressed image
	unsigned char rleheader;
	unsigned char length;
	rgba_t color = { 0, 0, 0, 0 };
	unsigned int i = 0;

	while(i < m_ImageSize)
	{
		fptr.read((char*)&rleheader,1);	// if error occurs, change it to 1
			
		if(rleheader >= 128)
		{
			// if the rle header >= 128 it means that we have a string of rle-127 pixels
		    length = (unsigned char)(rleheader - 127);
			
			//next 3 or 4 are the repeated values
			fptr.read((char*)&color.b, 1);
			fptr.read((char*)&color.g, 1);
			fptr.read((char*)&color.r, 1);
			
			if(colorMode == 4)
			{
				fptr.read((char*) &color.a, 1); 
				
			}

			// save this data to pImageData
			while(length > 0)
			{
				m_pImageData[i++] = color.b;
				m_pImageData[i++] = color.g;
				m_pImageData[i++] = color.r;

				if(colorMode == 4)
					m_pImageData[i++] = color.a;

				--length;
			}
		}	
		// if rleheader < 128, means the there is raw data following with rleheader + 1 pixels
		else
		{
			length = (unsigned char)(rleheader + 1);
			
			while(length > 0)
			{
				fptr.read((char*)&color.b, 1);
				fptr.read((char*)&color.g, 1);
				fptr.read((char*)&color.r, 1);
				
				if(colorMode == 4)
					fptr.read((char*)&color.a,1);

				m_pImageData[i++] = color.b;
				m_pImageData[i++] = color.g;
				m_pImageData[i++] = color.r;

				if(colorMode == 4)
					m_pImageData[i++] = color.a;

				--length;
			}
		}

	}

}

fptr.close();

// assign image data format
switch(tgaImageHeader.imageTypeCode)
{
case TGA_RGB:
case TGA_RGB_RLE: 
					if(3 == colorMode)
					{
						m_ColorDepth = 24;
						m_ImageDataFormat = IMAGE_RGB;
						m_ImageDataType = IMAGE_DATA_UNSIGNED_BYTE;
					}
					else
					{
						m_ColorDepth = 32;
						m_ImageDataFormat = IMAGE_RGBA;
						m_ImageDataType = IMAGE_DATA_UNSIGNED_BYTE;
					}
					break;

case TGA_GRAYSCALE:
case TGA_GRAYSCALE_RLE:
							m_ColorDepth = 8;
							m_ImageDataFormat = IMAGE_LUMINANCE;
							m_ImageDataType = IMAGE_DATA_UNSIGNED_BYTE;
							break;
}

if((tgaImageHeader.imageDesc & TOP_LEFT) == TOP_LEFT)
	FlipImage();	

 // finally swap the red and blue components...because the image is stored in that format
 SwapRedBlue();

 return (m_pImageData != NULL);

}

bool CTargaImage::FlipImage()
{
	if(!m_pImageData)
		return false;

	if(m_ColorDepth == 32)
	{
		rgba_t* tempBits = new rgba_t[m_Width];
		if(!tempBits)
			return false;

		int rowWidth = m_Width * 4;  // because there are 4 components : r,g,b,a

		rgba_t* top = (rgba_t*)m_pImageData;
		rgba_t* bottom = (rgba_t*)(m_pImageData + rowWidth * (m_Height - 1));

		// swap
		for(int i = 0; i < (m_Height / 2); ++i)
		{
			memcpy(tempBits,top,rowWidth);
			memcpy(top,bottom,rowWidth);
			memcpy(bottom,tempBits,rowWidth);

			top = (rgba_t*)((unsigned char*)top + rowWidth);
			bottom = (rgba_t*)((unsigned char*)bottom - rowWidth);
		}

		delete [] tempBits;
		tempBits = 0;
	}

	else if(m_ColorDepth == 24)
	{
		rgb_t* tempBits = new rgb_t[m_Width];
		if(!tempBits)
			return false;

		int rowWidth = m_Width * 3;  // because there are 3 components : r,g,b

		rgb_t* top = (rgb_t*)m_pImageData;
		rgb_t* bottom = (rgb_t*)(m_pImageData + rowWidth * (m_Height - 1));

		// swap
		for(int i = 0; i < (m_Height / 2); ++i)
		{
			memcpy(tempBits,top,rowWidth);
			memcpy(top,bottom,rowWidth);
			memcpy(bottom,tempBits,rowWidth);

			top = (rgb_t*)((unsigned char*)top + rowWidth);
			bottom = (rgb_t*)((unsigned char*)bottom - rowWidth);
		}

		delete [] tempBits;
		tempBits = 0;
	}

	return true;
}

void CTargaImage::Release()
{
   delete [] m_pImageData;
   m_pImageData = NULL;
}

bool CTargaImage::ConvertRGBAToRGB()
{
	if((m_ColorDepth == 32) && (m_ImageDataFormat == IMAGE_RGBA))
	{
		rgb_t* newImage = new rgb_t[m_Width * m_Height];

		if(!newImage)
			return false;

		rgb_t* dest = newImage;
		rgb_t* src = (rgb_t*)m_pImageData;

		for(int x = 0; x < m_Height; x++)	// becuz its stored in bgr format
		{
			for(int y = 0; y < m_Width; y++)
			{
				dest->r = src->r;
				dest->g = src->g;
				dest->b = src->b;
	
				++src;
				++dest;
			}
		}

		delete [] m_pImageData;
		m_pImageData = (unsigned char*)newImage;

		m_ColorDepth = 24;
		m_ImageDataFormat = IMAGE_RGB;
		m_ImageDataType = IMAGE_DATA_UNSIGNED_BYTE;

		return true;
	}

	return false;

}

bool CTargaImage::ConvertRGBToRGBA(unsigned char alphaValue)
{
	
	if((m_ColorDepth == 24) && (m_ImageDataFormat == IMAGE_RGB))
	{
		rgba_t* newImage = new rgba_t[m_Width * m_Height];

		if(!newImage)
			return false;

		rgba_t* dest = newImage;
		rgba_t* src = (rgba_t*)m_pImageData;

		for(int x = 0; x < m_Height; x++)	// becuz its stored in bgr format
		{
			for(int y = 0; y < m_Width; y++)
			{
				dest->r = src->r;
				dest->g = src->g;
				dest->b = src->b;
				dest->a = alphaValue;

				++src;
				++dest;
			}
		}

		delete [] m_pImageData;
		m_pImageData = (unsigned char*)newImage;

		m_ColorDepth = 32;
		m_ImageDataFormat = IMAGE_RGBA;
		m_ImageDataType = IMAGE_DATA_UNSIGNED_BYTE;

		return true;
	}

	return false;
}